package websockets

import (
	"encoding/json"
	"gitlab.com/mvenezia/kless-ui/pkg/websockets/models"
)

func CreateWebSocketMessage(messageType string, message string) []byte {
	output, _ := json.Marshal(wsmodels.WebSocketMessage{
		Type:    messageType,
		Message: message,
	})
	return output
}
