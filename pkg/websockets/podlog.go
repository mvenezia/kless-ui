package websockets

import (
	"encoding/json"
	"gitlab.com/mvenezia/kless-ui/pkg/websockets/models"
)

func CreatePodLogStreamReply(action string, status string, name string, container string) []byte {
	nestedBytes, _ := json.Marshal(wsmodels.PodLogStreamSubscriptionReply{
		Action:    action,
		Status:    status,
		Name:      name,
		Container: container,
	})
	return CreateWebSocketMessage("podLogStream", string(nestedBytes))
}

func CreatePodLogMessage(podName string, message string) []byte {
	nestedBytes, _ := json.Marshal(wsmodels.PodLogMessage{
		Pod:     podName,
		Message: message,
	})
	return CreateWebSocketMessage("podLogMessage", string(nestedBytes))
}
