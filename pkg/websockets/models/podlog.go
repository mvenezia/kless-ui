package wsmodels

type PodLogStreamSubscriptionCommand struct {
	Action    string `json:"action"`
	Name      string `json:"name"`
	Container string `json:"container"`
}

type PodLogStreamSubscriptionReply struct {
	Action    string `json:"action"`
	Status    string `json:"status"`
	Name      string `json:"name"`
	Container string `json:"container"`
}

type PodLogMessage struct {
	Pod     string `json:"pod"`
	Message string `json:"message"`
}
