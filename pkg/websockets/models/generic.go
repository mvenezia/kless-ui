package wsmodels

type WebSocketMessage struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}
