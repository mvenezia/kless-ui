package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"golang.org/x/net/context"
)

func GetRuntimeList() (reply *pb.GetEngineListReply, err error) {
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.GetEngineList(context.Background(), &pb.GetEngineListMsg{})
	return
}
