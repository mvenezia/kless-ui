package klessapi

import (
	"errors"
	"fmt"
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"os"
)

func getKlessAPIClient() (client pb.KlessAPIClient, conn *grpc.ClientConn, err error) {
	address := os.Getenv("KLESS_GRPC_ADDRESS")
	if address == "" {
		err = errors.New("Do not have address set for KLESS API, please set KLESS_GRPC_ADDRESS environmnental variable")
		return
	}
	port := os.Getenv("KLESS_GRPC_PORT")
	if port == "" {
		err = errors.New("Do not have address set for KLESS API, please set KLESS_GRPC_PORT environmnental variable")
		return
	}
	caFilename := os.Getenv("KLESS_GRPC_CA_PEM")
	serverAddr := fmt.Sprintf("%s:%s", address, port)

	if caFilename != "" {
		// Set up a connection to the server.
		creds, _ := credentials.NewClientTLSFromFile(caFilename, "")
		conn, err = grpc.Dial(serverAddr, grpc.WithTransportCredentials(creds))
	} else {
		conn, err = grpc.Dial(serverAddr, grpc.WithInsecure())
	}

	if err != nil {
		return
	}
	client = pb.NewKlessAPIClient(conn)
	return
}

func closeKlessAPIClient(connection *grpc.ClientConn) (bool, error) {
	err := connection.Close()
	if err != nil {
		return true, nil
	} else {
		return false, err
	}
}
