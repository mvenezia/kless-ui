package models

type UpdateFunction struct {
	Name     string       `json:"name"`
	Function FunctionBody `json:"function"`
}

type CreateFunction struct {
	Name        string          `json:"name"`
	Function    FunctionBody    `json:"function"`
	HTTPTrigger HTTPTriggerBody `json:"http_trigger"`
}

type FunctionBody struct {
	Deps     string `json:"deps"`
	Function string `json:"function"`
	Handler  string `json:"handler"`
	Runtime  string `json:"runtime"`
}

type HTTPTriggerBody struct {
	Gateway   string `json:"gateway"`
	Hostname  string `json:"hostname"`
	TLS       bool   `json:"tls"`
	TLSSecret string `json:"tls_secret"`
}
