package models

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
)

type PodLogAPIStreamMessage struct {
	LogMessage *pb.StreamPodLogReply
	Err        error
}
