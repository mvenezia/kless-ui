package klessapi

import (
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api/models"
	"golang.org/x/net/context"
)

func GetFunctionList() (reply *pb.GetFunctionListReply, err error) {
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.GetFunctionList(context.Background(), &pb.GetFunctionListMsg{})
	return
}

func GetFunction(name string) (reply *pb.GetFunctionReply, err error) {
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.GetFunction(context.Background(), &pb.GetFunctionMsg{Name: name})
	return
}

func DeleteFunction(name string) (reply *pb.GenericReply, err error) {
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.DeleteFunction(context.Background(), &pb.DeleteFunctionMsg{Name: name})
	return
}

func CreateFunction(input *models.CreateFunction) (reply *pb.GenericReply, err error) {
	data := &pb.CreateFunctionMsg{
		Name: input.Name,
		Function: &pb.FunctionMsg{
			Deps:     input.Function.Deps,
			Function: input.Function.Function,
			Handler:  input.Function.Handler,
			Runtime:  input.Function.Runtime,
		},
		HttpTrigger: &pb.CreateFunctionMsg_HTTPTriggerMsg{
			Gateway:   input.HTTPTrigger.Gateway,
			Hostname:  input.HTTPTrigger.Hostname,
			Tls:       input.HTTPTrigger.TLS,
			TlsSecret: input.HTTPTrigger.TLSSecret,
		},
	}
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.CreateFunction(context.Background(), data)
	return
}

func UpdateFunction(input *models.UpdateFunction) (reply *pb.GenericReply, err error) {
	data := &pb.UpdateFunctionMsg{
		Name: input.Name,
		Function: &pb.FunctionMsg{
			Deps:     input.Function.Deps,
			Function: input.Function.Function,
			Handler:  input.Function.Handler,
			Runtime:  input.Function.Runtime,
		},
	}
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}
	defer closeKlessAPIClient(connection)

	reply, err = apiClient.UpdateFunction(context.Background(), data)
	return
}
