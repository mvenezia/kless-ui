package klessapi

import (
	"context"
	"encoding/json"
	"fmt"
	pb "gitlab.com/mvenezia/kless-api/pkg/generated/api"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api/models"
	"gitlab.com/mvenezia/kless-ui/pkg/websockets"
	"google.golang.org/grpc"
	"io"
	"log"
)

func GeneratePodLogStreamAPIClient(name string) (stream pb.KlessAPI_StreamPodLogClient, connection *grpc.ClientConn, err error) {
	apiClient, connection, err := getKlessAPIClient()
	if err != nil {
		return
	}

	stream, err = apiClient.StreamPodLog(context.Background(), &pb.StreamPodLogMsg{Name: name})
	if err != nil {
		return
	}
	return
}

func CreatePodLogStream(podName string, containerName string, control <-chan string, output chan []byte) error {
	streamChannel := make(chan models.PodLogAPIStreamMessage)
	stream, _, err := GeneratePodLogStreamAPIClient(podName)
	if err != nil {
		// An error happening trying to create the client - need to bail!
		// TODO Probably should communicate this back upstream so that it can be re-added at some piont
		output <- websockets.CreatePodLogStreamReply("Add", fmt.Sprintf("Error: %s", err), podName, containerName)
		return err
	}

	// Letting the system know we added the subscription
	output <- websockets.CreatePodLogStreamReply("Add", "OK", podName, containerName)

	// When it is time to return to dust, we need to close the stream context - this will catch the go routine that is blocking
	defer func() {
		stream.Context().Done()
		log.Printf("Stream %s is dying\n", podName)
	}()

	// Creating a blocking go routine that is waiting on steam data
	go func() {
		data, err := stream.Recv()
		streamChannel <- models.PodLogAPIStreamMessage{LogMessage: data, Err: err}
	}()

	for {
		select {
		case <-control:
			// If we are told to go away, we will confirm it
			output <- websockets.CreatePodLogStreamReply("Delete", "OK", podName, containerName)
			log.Printf("Stopping the stream on pod %s and container %s\n", podName, containerName)
			return nil
		case data := <-streamChannel:
			// We got a message from the API - let's check it and forward it on
			if data.Err == io.EOF {
				break
			} else if data.Err != nil {
				output <- websockets.CreatePodLogStreamReply("Delete", fmt.Sprintf("Error: %s", data.Err), podName, containerName)
				log.Printf("write: %v", err)
				break
			} else if data.LogMessage.Log != nil {
				realMessage := string("")
				json.Unmarshal(data.LogMessage.Log, &realMessage)
				output <- websockets.CreatePodLogMessage(podName, realMessage)
			}
			// We need to receive more data, so start a go func again
			go func() {
				data, err := stream.Recv()
				streamChannel <- models.PodLogAPIStreamMessage{LogMessage: data, Err: err}
			}()
		}
	}
}
