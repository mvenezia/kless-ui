require("expose-loader?$!expose-loader?jQuery!jquery");
require("bootstrap-sass/assets/javascripts/bootstrap.js");
import axios from 'axios'
import VueAxios from 'vue-axios'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

import VueCodemirror from 'vue-codemirror'
import VueClipboard from 'vue-clipboard2'

// require styles
import 'codemirror/lib/codemirror.css'

import Vue from "vue";
import VueRouter from "router";
Vue.use(VueRouter);
Vue.use(VueCodemirror)
Vue.use(VueAxios, axios)
Vue.use(VueClipboard)
Vue.config.productionTip = false

Vue.use(VueMaterial)

import VueNativeSock from 'vue-native-websocket'
Vue.use(VueNativeSock, `ws://${window.location.host}/dummy`, { format: 'json'})

// import BandComponent from "./components/band.vue";
import App from "./components/app.vue";

import 'codemirror/mode/clike/clike.js'
import 'codemirror/mode/go/go.js'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/mode/php/php.js'
import 'codemirror/mode/properties/properties.js'
import 'codemirror/mode/python/python.js'
import 'codemirror/mode/ruby/ruby.js'
import 'codemirror/mode/toml/toml.js'
import 'codemirror/mode/xml/xml.js'
import 'codemirror/theme/darcula.css'

import AU from 'ansi_up'
const routes = [
  {path: "/", component: App, props: true}
];

const router = new VueRouter({
  mode: "history",
  routes
});

const app = new Vue(
{
    router,
    data: {
        runtimeList: [],
        functionList: [],
        podLog: ''
    },
    methods: {
        loadFunctionList () {
            console.log('I am loading the function list')
            this.axios
                .get('/api/v1/function/list')
                .then(response => {
                    if (response.data.ok === true) {
                        this.functionList = response.data.entries
                    } else {
                        alert('Could not load function list -->' + response.data.error + '<--')
                    }
                })
            setTimeout(this.loadFunctionList, 5000)
        },
        loadEngineList () {
            console.log('I am loading the runtime list')
            this.axios
                .get('/api/v1/runtime/list')
                .then(response => {
                    if (response.data.engines.length > 0) {
                        this.runtimeList = response.data.engines
                    } else {
                        // alert('Could not load runtime list -->' + response.data.error + '<--')
                    }
                })

        },
        handleSocketMessage (data) {
          let message = JSON.parse(data.data)
          switch (message.type) {
            case 'podLogMessage': {
              this.addLogMessage(JSON.parse(message.message))
            }
          }
        },
        addLogMessage (data) {
          let ansi_up = new AU;
          if (data.message != '') {
            this.podLog += ansi_up.ansi_to_html(data.message)
          }
        },
        subscribeToSomething () {
          if (this.$socket.readyState === 1) {
            let innerSubscribeMessage = {'action': 'Add', 'name': 'kless-ui-945874848-l4rwq', 'container': ''}
            let innerSubscribeString = JSON.stringify(innerSubscribeMessage)
            let subscribeMessage = {'type': 'podLogStream', 'message': innerSubscribeString}
            this.$socket.sendObj(subscribeMessage)
          } else {
            console.log(this.$socket)
            setTimeout(this.subscribeToSomething, 500)
          }
        }
    },
    created: function () {
      this.$options.sockets.onmessage = function (data) {
        this.handleSocketMessage(data)
      }
      console.log("Hi Mike")
        this.loadEngineList()
        this.loadFunctionList()
      this.subscribeToSomething()

    },
    mounted: function () {
    }
}).$mount("#app");
