package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/pkg/errors"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api/models"
)

func FunctionListHandler(c buffalo.Context) error {
	data, err := klessapi.GetFunctionList()
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}

func GetFunctionHandler(c buffalo.Context) error {
	data, err := klessapi.GetFunction(c.Param("name"))
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}

func DeleteFunctionHandler(c buffalo.Context) error {
	data, err := klessapi.DeleteFunction(c.Param("name"))
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}

func CreateFunctionHandler(c buffalo.Context) error {
	input := &models.CreateFunction{}
	if err := c.Bind(input); err != nil {
		return errors.WithStack(err)
	}
	data, err := klessapi.CreateFunction(input)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}

func UpdateFunctionHandler(c buffalo.Context) error {
	input := &models.UpdateFunction{}
	if err := c.Bind(input); err != nil {
		return errors.WithStack(err)
	}
	data, err := klessapi.UpdateFunction(input)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}
