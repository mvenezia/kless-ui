package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/pkg/errors"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api"
)

func RuntimeListHandler(c buffalo.Context) error {
	data, err := klessapi.GetRuntimeList()
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(data))
}
