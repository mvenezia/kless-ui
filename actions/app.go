package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo/middleware"
	"github.com/gobuffalo/buffalo/middleware/csrf"
	"github.com/gobuffalo/buffalo/middleware/i18n"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/packr"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App
var T *i18n.Translator

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_klessui_session",
		})
		// Automatically redirect to SSL
		//app.Use(ssl.ForceSSL(secure.Options{
		//	SSLRedirect:     ENV == "production",
		//	SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
		//}))

		if ENV == "development" {
			app.Use(middleware.ParameterLogger)
		}

		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		app.Use(csrf.New)

		// Setup and use translations:
		var err error
		if T, err = i18n.New(packr.NewBox("../locales"), "en-US"); err != nil {
			app.Stop(err)
		}
		app.Use(T.Middleware())

		app.ServeFiles("/assets", assetsBox)

		app.GET("/api/v1/runtime/list", RuntimeListHandler)
		app.GET("/api/v1/function/list", FunctionListHandler)
		app.GET("/api/v1/function", GetFunctionHandler)
		app.PUT("/api/v1/function", CreateFunctionHandler)
		app.POST("/api/v1/function", UpdateFunctionHandler)
		app.DELETE("/api/v1/function", DeleteFunctionHandler)
		app.GET("/dummy", WebSocketHandler)

		app.Middleware.Skip(csrf.New, DeleteFunctionHandler)

		app.GET("/{path:.+}", HomeHandler)
		app.GET("/", HomeHandler)
	}

	return app
}
