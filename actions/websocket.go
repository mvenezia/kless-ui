package actions

import (
	"bytes"
	"encoding/json"
	"github.com/gobuffalo/buffalo"
	"github.com/gorilla/websocket"
	"gitlab.com/mvenezia/kless-ui/pkg/kless-api"
	"gitlab.com/mvenezia/kless-ui/pkg/websockets/models"
	"log"
	"time"
)

type Message struct {
	Original  string    `json:"original"`
	Formatted string    `json:"formatted"`
	Received  time.Time `json:"received"`
}

func WebSocketHandler(c buffalo.Context) error {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
	}
	connection, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		log.Print("upgrade:", err)
		return err
	}

	websocketGateway(connection)
	return nil
}

func websocketGateway(connection *websocket.Conn) {
	outputChannel := make(chan []byte)
	inputChannel := make(chan []byte)
	controlChannel := make(chan string)

	go readWebSocket(connection, inputChannel, controlChannel)
	go writeWebSocket(connection, outputChannel, controlChannel)

	podCommandChannel := make(chan *wsmodels.PodLogStreamSubscriptionCommand)
	podLogStreamControlChannel := make(chan string)

	go podLogStreamGateway(podLogStreamControlChannel, podCommandChannel, outputChannel)

	for {
		select {
		case message := <-inputChannel:
			{
				realMessage := wsmodels.WebSocketMessage{}
				err := json.Unmarshal(message, &realMessage)
				if err != nil {
					log.Printf("Error was: %v\n", err)
					break
				}
				if realMessage.Type == "podLogStream" {
					log.Printf("Got a log stream message\n")
					podLogMessage := &wsmodels.PodLogStreamSubscriptionCommand{}
					err := json.Unmarshal([]byte(realMessage.Message), &podLogMessage)
					if err == nil {
						podCommandChannel <- podLogMessage
					} else {
						log.Printf("Error was: -->%v<--", err)
					}
				} else {
					log.Printf("Message was: %v\n", realMessage)
				}
			}
		case controlMessage := <-controlChannel:
			{
				if controlMessage == "Die" {
					return
				}
			}
		}
	}
}

func readWebSocket(connection *websocket.Conn, socket chan []byte, controlChannel chan string) {
	defer func() {
		log.Printf("read web socket is dying\n")
		connection.Close()
		controlChannel <- "Die"
	}()
	//connection.SetReadLimit(1024)
	//connection.SetReadDeadline(time.Now().Add(60*time.Second))
	//connection.SetPongHandler(func(string) error { connection.SetReadDeadline(time.Now().Add(60*time.Second)); return nil })
	for {
		_, message, err := connection.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, []byte{'\n'}, []byte{' '}, -1))
		socket <- message
	}
}

func writeWebSocket(connection *websocket.Conn, socket chan []byte, controlChannel chan string) {
	defer func() {
		log.Printf("write web socket is dying\n")
		connection.Close()
		controlChannel <- "Die"
	}()
	for {
		select {
		case message, ok := <-socket:
			//connection.SetWriteDeadline(time.Now().Add(10 * time.Second))
			if !ok {
				// channel is closed
				connection.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := connection.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(socket)
			for i := 0; i < n; i++ {
				w.Write([]byte{'\n'})
				w.Write(<-socket)
			}

			if err := w.Close(); err != nil {
				return
			}
		}
	}
}

func podLogStreamGateway(controlChannel <-chan string, commandChannel chan *wsmodels.PodLogStreamSubscriptionCommand, outputChannel chan []byte) error {
	podControlChannels := make(map[string]*chan string)
	inputChannel := make(chan []byte)
	for {
		select {
		case command := <-commandChannel:
			{
				if command.Action == "Add" {
					if podControlChannels[command.Name] == nil {
						log.Printf("Adding socket for %s\n", command.Name)
						newChannel := make(chan string)
						podControlChannels[command.Name] = &newChannel
						go klessapi.CreatePodLogStream(command.Name, "", *podControlChannels[command.Name], inputChannel)
					}
				}
				if command.Action == "Delete" {
					if podControlChannels[command.Name] != nil {
						log.Printf("Deleting socket for %s\n", command.Name)
						close(*podControlChannels[command.Name])
						podControlChannels[command.Name] = nil
					}
				}
			}
		case data := <-inputChannel:
			{
				outputChannel <- data
			}
		case controlMessage := <-controlChannel:
			{
				if controlMessage == "Die" {
					for _, podChannel := range podControlChannels {
						*podChannel <- "Die"
					}
				}
			}
		}
	}
	return nil
}

func removeSomething(channel chan string) {

}
