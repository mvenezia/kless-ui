package main

import (
	"log"

	"gitlab.com/mvenezia/kless-ui/actions"
	_ "github.com/gobuffalo/buffalo/runtime"
)

func main() {
	app := actions.App()
	log.Fatal(app.Serve())
}
