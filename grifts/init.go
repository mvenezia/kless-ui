package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/mvenezia/kless-ui/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
